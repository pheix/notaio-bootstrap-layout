# Responsive layout for Notaio.ru

Responsive layout implemented with Bootstrap **v5.2.3** front-end for [Notaio.ru](https://notaio.ru).

## Requirements

https://gitlab.com/pheix/notaio-bootstrap-layout/-/blob/main/doc/landing-site-requirements.docx

## Brand colors:

1. **regular black** — `#222222`
1. **purple** — `#993366`
1. **blue** — `#0000FF`
1. **red** — `#FF0000`

## Credits

- Apotheosis webdev bureau — [on Twitter](https://twitter.com/ApopheozRu), [on Web](https://apopheoz.ru)

- Bootstrap [v5.2.3](https://getbootstrap.com/docs/5.2/getting-started/introduction/) framework

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
